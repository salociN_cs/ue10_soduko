﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace UE10Soduko
{
    public class Cell
    {
        public string Name { get; set; }
        public double value { get; set; }
        public int row { get; set; }
        public int col { get; set; }
        public int block { get; set; }
        public Label cellLabel { get; set; }
        public List<double> knownCells { get; set; }
        public List<double> missingCells { get; set; }
        public bool locked { get; set; }

        public void calcMissingCells()
        {
            int i;
            this.missingCells = new List<double>(new double[] { 1, 2, 3, 4, 5, 6, 7, 8, 9 });
            for (i=0;i< missingCells.Count;i++)
            {
                if (this.knownCells.Contains(missingCells[i]))
                {
                    missingCells[i] = 0;
                }
            }
            missingCells.RemoveAll(missingCells => missingCells == 0);
        }
    }
}
