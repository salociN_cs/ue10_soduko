﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
//using System.Threading.Tasks;
using System.Windows.Forms;
using System.Collections;
using System.IO;

namespace UE10Soduko
{
    public partial class Form1 : Form
    {
        public ArrayList controlItems = new ArrayList();
        static public int gridSize = 9;
        static public int iMax = gridSize * gridSize;
        public List<Cell> cells = new List<Cell>();
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {


            this.AutoSize = true;
            this.MaximizeBox = false;
            this.AutoSizeMode = AutoSizeMode.GrowAndShrink;

            int fontSize = 25;
            int cellSize = fontSize * 3;
            int i;
            int x = 0;
            int y = 50;
            int yDefault = y;
            int row = 0;
            int col = 0;

            //create cells
            for (i = 0; i < iMax; i++)
            {
                Cell newCell = new Cell();
                newCell.cellLabel = new Label();
                newCell.Name = Convert.ToString(i);
                newCell.value = 0;
                newCell.locked = false;
                //Berechne Position auf x & y Achse
                if (i % gridSize == 0)
                {
                    row++;
                    col = 0;
                }
                newCell.row = row;
                col++;
                newCell.col = col;
                //Berechne im welchen Feld/Block diese Zelle liegt
                if (col < 4 && row < 4)
                    newCell.block = 1;
                else if (col > 3 && col < 7 && row < 4)
                    newCell.block = 2;
                else if (col > 6 && row < 4)
                    newCell.block = 3;
                else if (col < 4 && row > 3 && row < 7)
                    newCell.block = 4;
                else if (col > 3 && col < 7 && row > 3 && row < 7)
                    newCell.block = 5;
                else if (col > 6 && row > 3 && row < 7)
                    newCell.block = 6;
                else if (col < 4 && row > 6)
                    newCell.block = 7;
                else if (col > 3 && col < 7 && row > 6)
                    newCell.block = 8;
                else if (col > 6 && row > 6)
                    newCell.block = 9;

                //erstelle das Label zur visuellen ansicht
                if (newCell.value.Equals(0))
                {
                    newCell.cellLabel.Text = "";
                }
                else
                {
                    newCell.cellLabel.Text = Convert.ToString(newCell.value);
                }
                if (i % gridSize == 0 && i > 1)
                {
                    x = 0;
                    y += cellSize;
                }
                //setze raum zwischen 3x3 feldern
                int space = 3;
                if (x == cellSize * 3 || x == cellSize * 6 + space)
                {
                    x += space;
                }
                if (y == yDefault + cellSize * 3 || y == yDefault + cellSize * 6 + space)
                {
                    y += space;
                }
                newCell.cellLabel.Name = Convert.ToString(i);
                newCell.cellLabel.Location = new Point(x, y);
                newCell.cellLabel.Font = new Font("Times New Roman", fontSize);
                newCell.cellLabel.TextAlign = ContentAlignment.MiddleCenter;
                newCell.cellLabel.Size = new Size(cellSize, cellSize);
                newCell.cellLabel.BorderStyle = BorderStyle.FixedSingle;
                newCell.cellLabel.BackColor = Color.White;
                newCell.cellLabel.Click += new EventHandler(selectNumber);
                //Füge das label der Zelle den Controls hinzu
                Controls.Add(newCell.cellLabel);
                //Füge die zelle den cells hinzu
                cells.Add(newCell);
                //setze x um zellengröße hoch für das nächste label
                x += cellSize;
            }

            //add controls to controlsItems
            i = 0;
            foreach (var ControlItem in Controls)
            {
                controlItems.Add(ControlItem);
            }
        }

        private void ladenToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string pattern;
            int i;

            var filePath = string.Empty;
            using (OpenFileDialog openFileDialog = new OpenFileDialog())
            {
                openFileDialog.InitialDirectory = "D:\\Projekte\\UE10Soduko\\pattern\\";
                openFileDialog.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
                openFileDialog.FilterIndex = 2;
                openFileDialog.RestoreDirectory = true;

                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    filePath = openFileDialog.FileName;
                }
            }
            if (!filePath.Equals(string.Empty))
            {
                using (StreamReader sr = new StreamReader(filePath))
                {
                    pattern = sr.ReadToEnd();
                }
                if (cells.Count == pattern.Length)
                {
                    i = 0;
                    foreach (var cell in cells)
                    {
                        cell.value = char.GetNumericValue(pattern[i]);
                        if (cell.value.Equals(0))
                        {
                            cell.locked = false;
                            cell.cellLabel.BackColor = Color.White;
                            cell.cellLabel.Text = "";
                        }
                        else
                        {
                            cell.locked = true;
                            cell.cellLabel.BackColor = Color.LightGray;
                            cell.cellLabel.Text = Convert.ToString(cell.value);
                        }
                        i++;
                    }
                }
            }
        }

        private void tESTToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (var cell in cells)
            {
                //Console.WriteLine("------------------------------------");
                //Console.WriteLine("VALUE: " +cell.value);
                //Console.WriteLine("row: " +cell.row);
                //Console.WriteLine("col: " +cell.col);
                //Console.WriteLine("block: " +cell.block);
            }
        }

        private void loesenToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int emptyFieldsCount;
            List<Cell> emptyFieldsList = cells.Where(x => x.value.Equals(0)).ToList();
            do
            {
                emptyFieldsCount = emptyFieldsList.Count;
                foreach (Cell sCell in emptyFieldsList)
                {
                    sCell.knownCells = new List<double>();
                    cells.Where(x => x.col.Equals(sCell.col)).ToList().ForEach(s =>
                    {
                        if (!sCell.knownCells.Contains(s.value) && !s.value.Equals(0.0))
                        {
                            sCell.knownCells.Add(s.value);
                        }
                    });

                    cells.Where(x => x.row.Equals(sCell.row)).ToList().ForEach(s =>
                    {
                        if (!sCell.knownCells.Contains(s.value) && !s.value.Equals(0.0))
                        {
                            sCell.knownCells.Add(s.value);
                        }
                    });

                    cells.Where(x => x.block.Equals(sCell.block)).ToList().ForEach(s =>
                    {
                        if (!sCell.knownCells.Contains(s.value) && !s.value.Equals(0.0))
                        {
                            sCell.knownCells.Add(s.value);
                        }
                    });
                    sCell.calcMissingCells();
                    if (sCell.missingCells.Count() == 1) //wenn nur eine zahl fehlt, muss diese in das feld geschrieben werden
                    {
                        sCell.value = Convert.ToDouble(sCell.missingCells.First());
                        sCell.cellLabel.Text = Convert.ToString(sCell.value);
                    }
                }
                emptyFieldsList = cells.Where(x => x.value.Equals(0.0)).ToList();
                if (emptyFieldsCount == emptyFieldsList.Count)//damit keine Dauerschleife entsteht wird geprüft ob ein Feld gelösst werden konnte/ wenn keine zahl mehr gelösst wurde gehe hierein
                {
                    //gehe zum nächsten leeren feld und setzte die nächst mögliche zahl  ein 
                    //starte die normale prüfung
                    //prüfe die genaration ob sie erlaubt/korrekt ist
                    //wenn Ja. fahre fort
                    //wenn Nein. ändere probiere die nächste zahl
                    //etc.
                    /*Cell cellLowestCount = new Cell();
                        if (tempCount < lowestCount)
                        {
                            cellLowestCount = sCell;
                            lowestCount = tempCount;
                        }
                        */

                    //cellLowestCount.value = 1.0;
                    //cellLowestCount.cellLabel.Text = "1";

                    int lowestCount = 9999;
                    Cell cellLowestCount = new Cell();
                    foreach (Cell sCell in emptyFieldsList)
                    {
                        if (sCell.missingCells.Count < lowestCount)
                        {
                            cellLowestCount = sCell;
                            lowestCount = sCell.missingCells.Count;
                        }
                    }
                    //Console.WriteLine("Zelle mit wenigsten möglichkeiten: " +cellLowestCount.Name +" - Anzahl Möglichkeiten: " +cellLowestCount.missingCells.Count);

                    foreach (double cellValue in cellLowestCount.missingCells)
                    {
                        cellLowestCount.value = cellValue;
                        cellLowestCount.cellLabel.Text = Convert.ToString(cellLowestCount.value);
                        //weiter solang bis nicht mehr eindeutig ..dann next

                    }



                    //die Zelle mit den wenigsten möglichen feldern zwischen speichern
                    //MessageBox.Show("Ich bin nur hier damit hier ein Debug-Punkt entstehen kann.");
                    //MessageBox.Show(lowestCount.ToString());
                    //MessageBox.Show(string.Format("Row: {0} - Cell: {1} - Block: {2}", cellLowestCount.row, cellLowestCount.col, cellLowestCount.block));
                    MessageBox.Show("Das Soduko konnte nicht gelösst werden.");
                    break;

                }
            } while (emptyFieldsList.Count > 0); //Solgang wie ein leeres feld existiert führe den code aus
        }

        private void selectNumber(object sender, EventArgs e)
        {
            Label currentLabel = sender as Label;
            foreach (var cell in cells)
            {
                if (cell.Name == currentLabel.Name)
                {
                    if (!cell.locked)
                    {
                        NumberSelector numberSel = new NumberSelector();
                        var result = numberSel.ShowDialog();
                        if (result == DialogResult.Cancel)
                        {
                            int returnValue;
                            switch (numberSel.returnValue)
                            {
                                case "labelNumber1":
                                    returnValue = 1;
                                    break;
                                case "labelNumber2":
                                    returnValue = 2;
                                    break;
                                case "labelNumber3":
                                    returnValue = 3;
                                    break;
                                case "labelNumber4":
                                    returnValue = 4;
                                    break;
                                case "labelNumber5":
                                    returnValue = 5;
                                    break;
                                case "labelNumber6":
                                    returnValue = 6;
                                    break;
                                case "labelNumber7":
                                    returnValue = 7;
                                    break;
                                case "labelNumber8":
                                    returnValue = 8;
                                    break;
                                case "labelNumber9":
                                    returnValue = 9;
                                    break;
                                case "labelEmpty":
                                    returnValue = 0;
                                    break;
                                default:
                                    returnValue = 99;
                                    break;
                            }
                            if (returnValue == 0)
                            {
                                cell.value = returnValue;
                                cell.cellLabel.Text = "";
                            }
                            else if (returnValue > 0 && returnValue < 10)
                            {
                                cell.value = returnValue;
                                cell.cellLabel.Text = Convert.ToString(returnValue);
                            }
                        }
                    }
                }
            }
        }
    }
}

/*

Aufgabe 1: 
    - Durchläuft der Reihenach alle leeren Felder
    - schreibt alle zahlen aus den anderen Felder die  Entweder in der selben Reihe, 
      Spalte oder im selben Block liegen in eine Liste (wenn eine zahl im array bereits vorhanden ist, wird diese nicht hinzugefügt)
    - wenn genau 9(eigentlich 8, aber 0 wird auch in die liste geschrieben) elemente in der liste vorhanden sind, kann das leere feld mit 
      der noch fehlende zahl gefüllt werden
    - Grenze:
        Wenn es kein leeres Feld gibt, dass eindeutig zu ermittel ist anhand der Zahlen aus der selben Reihe,Spalte und in dem Block
        Erweiterung: 
            wenn keine zahl mehr ermittelt werden kann, muss eine Zahl eingesetzt werden und beim Fehler muss zurück gegangen werden zu einem stand der korrekt ist
    - Unterschiede der Schwierigskeitgerade
        Eindeutige Felder & Felder die anhand von ausschlussverfahren ermittelt werden
Aufgabe 2:
    - Erweiterung
Aufgabe 3: 
    - Struktogramm
    - Vortrag
     
     
     
     
     
*/
