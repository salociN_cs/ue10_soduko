﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace UE10Soduko
{
    public partial class NumberSelector : Form
    {
        public string returnValue { get; set; }
        public NumberSelector()
        {
            InitializeComponent();
        }

        private void NumberSelector_Load(object sender, EventArgs e)
        {

        }

        private void labelNumber_Click(object sender, EventArgs e)
        {
            Label labelNumber = sender as Label;
            returnValue = labelNumber.Name;
            this.Close();
        }
    }
}
