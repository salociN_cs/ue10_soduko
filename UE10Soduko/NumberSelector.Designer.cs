﻿namespace UE10Soduko
{
    partial class NumberSelector
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelNumber7 = new System.Windows.Forms.Label();
            this.labelNumber8 = new System.Windows.Forms.Label();
            this.labelNumber9 = new System.Windows.Forms.Label();
            this.labelNumber4 = new System.Windows.Forms.Label();
            this.labelNumber5 = new System.Windows.Forms.Label();
            this.labelNumber6 = new System.Windows.Forms.Label();
            this.labelNumber3 = new System.Windows.Forms.Label();
            this.labelNumber2 = new System.Windows.Forms.Label();
            this.labelNumber1 = new System.Windows.Forms.Label();
            this.labelEmpty = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // labelNumber7
            // 
            this.labelNumber7.BackColor = System.Drawing.SystemColors.Window;
            this.labelNumber7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelNumber7.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNumber7.Location = new System.Drawing.Point(12, 9);
            this.labelNumber7.Name = "labelNumber7";
            this.labelNumber7.Size = new System.Drawing.Size(35, 35);
            this.labelNumber7.TabIndex = 0;
            this.labelNumber7.Text = "7";
            this.labelNumber7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelNumber7.Click += new System.EventHandler(this.labelNumber_Click);
            // 
            // labelNumber8
            // 
            this.labelNumber8.BackColor = System.Drawing.SystemColors.Window;
            this.labelNumber8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelNumber8.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNumber8.Location = new System.Drawing.Point(53, 9);
            this.labelNumber8.Name = "labelNumber8";
            this.labelNumber8.Size = new System.Drawing.Size(35, 35);
            this.labelNumber8.TabIndex = 1;
            this.labelNumber8.Text = "8";
            this.labelNumber8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelNumber8.Click += new System.EventHandler(this.labelNumber_Click);
            // 
            // labelNumber9
            // 
            this.labelNumber9.BackColor = System.Drawing.SystemColors.Window;
            this.labelNumber9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelNumber9.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNumber9.Location = new System.Drawing.Point(94, 9);
            this.labelNumber9.Name = "labelNumber9";
            this.labelNumber9.Size = new System.Drawing.Size(35, 35);
            this.labelNumber9.TabIndex = 2;
            this.labelNumber9.Text = "9";
            this.labelNumber9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelNumber9.Click += new System.EventHandler(this.labelNumber_Click);
            // 
            // labelNumber4
            // 
            this.labelNumber4.BackColor = System.Drawing.SystemColors.Window;
            this.labelNumber4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelNumber4.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNumber4.Location = new System.Drawing.Point(12, 55);
            this.labelNumber4.Name = "labelNumber4";
            this.labelNumber4.Size = new System.Drawing.Size(35, 35);
            this.labelNumber4.TabIndex = 3;
            this.labelNumber4.Text = "4";
            this.labelNumber4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelNumber4.Click += new System.EventHandler(this.labelNumber_Click);
            // 
            // labelNumber5
            // 
            this.labelNumber5.BackColor = System.Drawing.SystemColors.Window;
            this.labelNumber5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelNumber5.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNumber5.Location = new System.Drawing.Point(53, 55);
            this.labelNumber5.Name = "labelNumber5";
            this.labelNumber5.Size = new System.Drawing.Size(35, 35);
            this.labelNumber5.TabIndex = 4;
            this.labelNumber5.Text = "5";
            this.labelNumber5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelNumber5.Click += new System.EventHandler(this.labelNumber_Click);
            // 
            // labelNumber6
            // 
            this.labelNumber6.BackColor = System.Drawing.SystemColors.Window;
            this.labelNumber6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelNumber6.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNumber6.Location = new System.Drawing.Point(94, 55);
            this.labelNumber6.Name = "labelNumber6";
            this.labelNumber6.Size = new System.Drawing.Size(35, 35);
            this.labelNumber6.TabIndex = 5;
            this.labelNumber6.Text = "6";
            this.labelNumber6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelNumber6.Click += new System.EventHandler(this.labelNumber_Click);
            // 
            // labelNumber3
            // 
            this.labelNumber3.BackColor = System.Drawing.SystemColors.Window;
            this.labelNumber3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelNumber3.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNumber3.Location = new System.Drawing.Point(94, 99);
            this.labelNumber3.Name = "labelNumber3";
            this.labelNumber3.Size = new System.Drawing.Size(35, 35);
            this.labelNumber3.TabIndex = 8;
            this.labelNumber3.Text = "3";
            this.labelNumber3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelNumber3.Click += new System.EventHandler(this.labelNumber_Click);
            // 
            // labelNumber2
            // 
            this.labelNumber2.BackColor = System.Drawing.SystemColors.Window;
            this.labelNumber2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelNumber2.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNumber2.Location = new System.Drawing.Point(53, 99);
            this.labelNumber2.Name = "labelNumber2";
            this.labelNumber2.Size = new System.Drawing.Size(35, 35);
            this.labelNumber2.TabIndex = 7;
            this.labelNumber2.Text = "2";
            this.labelNumber2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelNumber2.Click += new System.EventHandler(this.labelNumber_Click);
            // 
            // labelNumber1
            // 
            this.labelNumber1.BackColor = System.Drawing.SystemColors.Window;
            this.labelNumber1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelNumber1.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNumber1.Location = new System.Drawing.Point(12, 99);
            this.labelNumber1.Name = "labelNumber1";
            this.labelNumber1.Size = new System.Drawing.Size(35, 35);
            this.labelNumber1.TabIndex = 6;
            this.labelNumber1.Text = "1";
            this.labelNumber1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelNumber1.Click += new System.EventHandler(this.labelNumber_Click);
            // 
            // labelEmpty
            // 
            this.labelEmpty.BackColor = System.Drawing.SystemColors.Window;
            this.labelEmpty.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelEmpty.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelEmpty.Location = new System.Drawing.Point(12, 146);
            this.labelEmpty.Name = "labelEmpty";
            this.labelEmpty.Size = new System.Drawing.Size(117, 35);
            this.labelEmpty.TabIndex = 9;
            this.labelEmpty.Text = "-";
            this.labelEmpty.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelEmpty.Click += new System.EventHandler(this.labelNumber_Click);
            // 
            // NumberSelector
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Window;
            this.ClientSize = new System.Drawing.Size(139, 188);
            this.Controls.Add(this.labelEmpty);
            this.Controls.Add(this.labelNumber3);
            this.Controls.Add(this.labelNumber2);
            this.Controls.Add(this.labelNumber1);
            this.Controls.Add(this.labelNumber6);
            this.Controls.Add(this.labelNumber5);
            this.Controls.Add(this.labelNumber4);
            this.Controls.Add(this.labelNumber9);
            this.Controls.Add(this.labelNumber8);
            this.Controls.Add(this.labelNumber7);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "NumberSelector";
            this.Text = "NumberSelector";
            this.Load += new System.EventHandler(this.NumberSelector_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label labelNumber7;
        private System.Windows.Forms.Label labelNumber8;
        private System.Windows.Forms.Label labelNumber9;
        private System.Windows.Forms.Label labelNumber4;
        private System.Windows.Forms.Label labelNumber5;
        private System.Windows.Forms.Label labelNumber6;
        private System.Windows.Forms.Label labelNumber3;
        private System.Windows.Forms.Label labelNumber2;
        private System.Windows.Forms.Label labelNumber1;
        private System.Windows.Forms.Label labelEmpty;
    }
}